module gitea.com/tango/session-nodb

go 1.20

require (
	gitea.com/lunny/log v0.0.0-20190322053110-01b5df579c4e
	gitea.com/lunny/nodb v0.0.0-20191109090517-8ed7dfb2f9ce
	gitea.com/lunny/tango v0.6.2
	gitea.com/tango/session v0.0.0-20201110080243-87f6e468e457
	github.com/ledisdb/ledisdb v0.0.0-20200510135210-d35789ec47e6
	github.com/syndtr/goleveldb v1.0.0
)

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/cupcake/rdb v0.0.0-20161107195141-43ba34106c76 // indirect
	github.com/edsrzf/mmap-go v0.0.0-20170320065105-0bce6a688712 // indirect
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/pelletier/go-toml v1.0.1 // indirect
	github.com/siddontang/go v0.0.0-20170517070808-cb568a3e5cc0 // indirect
	github.com/siddontang/go-snappy v0.0.0-20140704025258-d8f7bb82a96d // indirect
	github.com/siddontang/rdb v0.0.0-20150307021120-fc89ed2e418d // indirect
	golang.org/x/net v0.0.0-20180906233101-161cd47e91fd // indirect
)
