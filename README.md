# session-nodb

Session-nodb is a store of [session](https://gitea.com/tango/session) middleware for [Tango](https://gitea.com/lunny/tango) stored session data via [nodb](http://gitea.com/lunny/nodb). 

## Installation

    go get gitea.com/tango/session-nodb

## Simple Example

```Go
package main

import (
    "gitea.com/lunny/tango"
    "gitea.com/tango/session"
    "gitea.com/tango/session-nodb"
)

type SessionAction struct {
    session.Session
}

func (a *SessionAction) Get() string {
    a.Session.Set("test", "1")
    return a.Session.Get("test").(string)
}

func main() {
    o := tango.Classic()
    store, _ := nodbstore.New(nodbstore.Options{
        Path:    "./nodbstore",
        DbIndex: 0,
        MaxAge:  30 * time.Minute,
    })
    o.Use(session.New(session.Options{
        Store: store,
        }))
    o.Get("/", new(SessionAction))
}
```

## Getting Help

- [API Reference](https://pkg.go.dev/gitea.com/tango/session-nodb)
